import React, {useCallback, useState} from "react";
import CafeItem from "../components/CafeItem";
import CartItem from "../components/CartItem";

const TestPage1 = () => {
    const [cart, setCart] = useState([])

    const menus = [
        { name: '아메리카노', price: 3000, img: '/assets/americano.png'},
        { name: '카라멜 마끼아또', price: 4500, img: '/assets/caramel.png'},
        { name: '아이스티', price: 3500, img: '/assets/ice.png'},
        { name: '카페라떼', price: 4000, img: '/assets/latte.png'},
        { name: '레몬에이드', price: 4000, img: '/assets/lemonade.png'},
        { name: '카페모카', price: 4500, img: '/assets/mocha.png'},
    ]

    const addToCart = useCallback(menu => {
        setCart((prevCart) => {
            const existingItem = prevCart.find((item) => item.name === menu.name)
            if (existingItem) {
                return prevCart.map((item) =>
                item.name === menu.name ? {...item, quantity: item.quantity + 1} : item
                )
            } else {
                return [...prevCart, { ...menu, quantity: 1}]
            }
        })
    }, [])

    const removeFromCart = useCallback((menu) => {
        setCart((prevCart) => {
            return prevCart.reduce((acc, item) => {
                if (item.name === menu.name) {
                    if(item.quantity > 1) {
                        return [...acc, {...item, quantity: item.quantity - 1}]
                    }
                } else {
                    return [...acc, item]
                }
                return acc
            }, [])
        })
    }, [])

    const calTotal = useCallback(() => {
        return cart.reduce((total, item) => total + item.price * item.quantity, 0)
    }, [cart])

    return (
        <div>
            <h1>카페 메뉴</h1>
            <div className="menuFrame">
                {menus.map(menu => (
                    <CafeItem key={menu.name} menu={menu} addToCart={addToCart}/>
                ))}
            </div>
            <h2>장바구니</h2>
            {cart.map(item => (
                <CartItem key={item.name} item={item} removeFromCart={removeFromCart}/>
            ))}
            <p>총 금액: {calTotal()}원</p>
        </div>
    )
}

export default TestPage1