import React, {useEffect, useState} from "react";
import MenuCard from "../components/MenuCard";

const Ask = () => {
    const [basket, setBasket] = useState([])
    const [totalPrice, setTotalPrice] = useState(0)

    const menus = [
        { name: '아메리카노', price: 3000, img: '/assets/americano.png'},
        { name: '카라멜 마끼아또', price: 4500, img: '/assets/caramel.png'},
        { name: '아이스티', price: 3500, img: '/assets/ice.png'},
        { name: '카페라떼', price: 4000, img: '/assets/latte.png'},
        { name: '레몬에이드', price: 4000, img: '/assets/lemonade.png'},
        { name: '카페모카', price: 4500, img: '/assets/mocha.png'},
    ]

    useEffect(() => {
        localStorage.setItem('basket', JSON.stringify(basket))
    }, [basket]);

    const addBasket = (menu) => {
        setBasket([...basket, menu])
        setTotalPrice(totalPrice + menu.price)
    }

    const requestBasket = () => {
        alert('주문이 완료 되었습니다.')
        setBasket([])
        setTotalPrice(0)
    }

    return (
        <div className="mainContent">
            <h1>음료</h1>
            <section className="menuSection">
                {menus.map(menu => (
                    <MenuCard key={menu.name} menu={menu} addBasket={addBasket}/>
                ))}
            </section>
            <h1>장바구니</h1>
            <div className="result">
                <p>주문 메뉴 : {basket}</p>
                <p>총 주문 금액 : {totalPrice}</p>
            </div>
            <button onClick={requestBasket}>주문하기</button>
        </div>
    )
}

export default Ask