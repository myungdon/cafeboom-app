import React from "react";

const DefaultLayout = ({children}) => {
    return (
        <>
            <div className="mainHeader">Cafe Boom Boom</div>
            <main>{children}</main>
            <footer className="mainFooter">대표 : 홍길동 call : 000-0000</footer>
        </>
    )
}

export default DefaultLayout