import React from "react";

const CafeItem = ({ menu, addToCart}) => {
    return (
        <div onClick={() => addToCart(menu)}>
            <img src={process.env.PUBLIC_URL + menu.img} width='400px' height="400px"/>
            <div>{menu.name}</div>
            <div>{menu.price}</div>
        </div>
    )
}

export default CafeItem