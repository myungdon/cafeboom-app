import React from "react";
const MenuCard = ({ menu, addBasket }) => (
    <div onClick={() => addBasket(menu)}>
        <img src={process.env.PUBLIC_URL + menu.img} width='400px' height="400px"/>
        <h2>{menu.name}</h2>
        <p>{menu.price}원</p>
    </div>
)

export default MenuCard