import React from "react";

const CartItem = ({ item, removeFromCart }) => {
    return (
        <div>
            <h4>{item.name}</h4>
            <p>수량: {item.quantity}</p>
            <button onClick={() => removeFromCart(item)}>삭제</button>
        </div>
    )
}

export default CartItem