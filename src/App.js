import logo from './logo.svg';
import './App.css';
import {Route, Routes} from "react-router-dom";
import DefaultLayout from "./layouts/DefaultLayout";
import Ask from "./pages/Ask";
import TestPage1 from "./pages/TestPage1";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<DefaultLayout><TestPage1 /></DefaultLayout>} />
          <Route path="/1" element={<DefaultLayout><Ask /></DefaultLayout>} />
      </Routes>
    </div>
  );
}

export default App;
